#!/usr/bin/env/python
from core import libengine
import sys
import os
abspath = os.path.abspath(os.curdir)
print(abspath)
while "PyWright_run.app" in abspath:
    abspath = os.path.split(abspath)[0]
os.chdir(abspath)
f = open("logfile.txt", "a")
f.write("running from:" + abspath + "\n")
sys.stderr = f
sys.stdout = f
sys.path.insert(0, "")
libengine.run()
