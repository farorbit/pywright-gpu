# -*- mode: python ; coding: utf-8 -*-

block_cipher = None


a = Analysis(['PyWright.py'],
             pathex=['/home/usename/Downloads/pywright-master/core'],
             binaries=[],
             datas=[],
             hiddenimports=['pygame', 'simplejson', 'numpy', '[numpy.core]', 'requests', 'certifi'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='PyWright',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=False , icon='data/art/general/bb.ico')
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               upx_exclude=[],
               name='PyWright')
