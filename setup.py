from cx_Freeze import setup, Executable

includefiles = ["tools","music","fonts","examples","docs","art","sfx","core","main.py","gen_docs.py","PyWright.py","PyWright_run.py","updater.py","setup.py","README.md","requirements.txt","settings.ini","Pipfile","License.MD","lastlog.txt","lastgame","installer.cfg","doc.txt","data.txt","changelog.txt"]#"python3.dll","python39.dll",
# Dependencies are automatically detected, but it might need
# fine tuning.
buildOptions = {"include_files" : includefiles, "packages" : ["numpy"], "excludes" : ["lib2to3", "scipy", "PyQt4", "PyQt5", "matplotlib"], "optimize" : 1}#2 set to 1 for now bc 2 crashes cxfreeze and pyinstaller when using numpy#"email", 

import sys
base = 'Win32GUI' if sys.platform=='win32' else None

executables = [
    Executable('PyWright_run.py', base=base)#, icon="art/general/bb.ico")
]

setup(name='PyWright',
      version = '1.15.8',
      description = 'Gpu acceleration, python 3 pywright.',
      options = dict(build_exe = buildOptions),
      executables = executables)
